const express = require('express');
require('./db/mongoose');
const userRouter = require('./router/user');
const taskRouter = require('./router/task');

const app = express();
const port = process.env.PORT || 3000;

// app.use((req,res,next)=>{
//     if(req.method==="GET"){
//         res.send('GET request are disable')
//     }
//     else{
//         next();
//     }
// })
// app.use((req,res,next)=>{
    
//         res.status(503).send('Site is currently down. Check back soon!');
// })
app.use(express.json()); // converting to direct into json format
app.use(userRouter); //for external user routing 
app.use(taskRouter);



app.listen(port, ()=>{
    console.log('Server is up on port ' + port);
})




