require('../src/db/mongoose')
const User = require('../src/model/user')

// User.findByIdAndUpdate('5f92d770fe75122ba0684739', { age:1 }).then((user)=>{
//     console.log(user);
//     return User.countDocuments({ age:1 });
// }).then((result)=>{
//     console.log(result);
// }).catch((e)=>{
//     console.log(e);
// })

const updateAgeAndCount=async(id,age)=>{
    const user = await User.findByIdAndUpdate(id,{ age });
    const count = await User.countDocuments({age});
    return count;
}

updateAgeAndCount('5f95323d250d66603873ce05', 2).then((count)=>{
    console.log(count);
}).catch((e)=>{
    console.log(e);
})