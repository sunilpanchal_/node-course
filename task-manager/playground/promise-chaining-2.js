const Task = require('../src/model/task')
require('../src/db/mongoose')

// Task.findByIdAndDelete('5f93b5af338d1248c8bbb462').then((user)=>{
//     console.log(user);
//     return Task.countDocuments({completed:false});
// }).then((result)=>{
//     console.log(result);
// }).catch((e)=>{
//     console.log(e);
// })


const deleteAndCount=async(id,completed)=>{
    const deleteByID = await Task.findByIdAndDelete(id);
    const count = await Task.countDocuments({completed});
    
    return count;
}

deleteAndCount('5f9537a46c705b40fc425a93', false).then((count)=>{
    console.log(count);
}).catch((e)=>{
    console.log(e);
})

