const name = function(){
    return 'Normal function!!'
}

const arrowfun = ()=>{
    return 'Arrow function!!'
}

const event = {
    name: 'Birthday Party',
    guestList: ['Sunil','Ankit','Nitin'],
    printGuestList(){
        console.log('uesr list for '+ this.name )

        this.guestList.forEach((guest)=>{
            console.log(guest + ' is attending ' + this.name)
        })
    }
}
event.printGuestList()
