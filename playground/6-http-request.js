const http = require('http');
const url = 'http://api.openweathermap.org/data/2.5/weather?lat=27.2038&lon=77.5011&appid=7c4d5f4a25bff70f6f880e19f16ca3f6'

//for https you have to only relace http to https otherwise both are same


const request = http.request(url,(response)=>{
    let data = '';
    response.on('data',(chunk)=>{
        data = data + chunk.toString();
    })
    response.on('end',()=>{
        const body = JSON.parse(data);
        console.log(body);
    })
})

request.on('error',(error)=>{
    console.log('An error: ', error);
})


request.end();